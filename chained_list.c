#include "chained_list.h"
#include <stdio.h>
#include <stdlib.h>

/* Summary:
20 cons_li()
30 create_li()
49 affiche_li()


--Made by Drkn--


*/




//build a list cell
chained_list * cons_li(int info,chained_list * P)
{
	chained_list * L=malloc(sizeof(chained_list));
	L->info=info;
	L->succ=P;
	return L;
}


//create a complex list using cons_li
chained_list * create_li(int taille)
{
	chained_list * P=NULL;
	//chained_list L;
	int info;
	for(int i=0;i<taille;i++)
	{
		printf("Case n°%d :",i+1);
		scanf("%d",&info);

		P=ins_li(info,P);
	}
	return P;
}




//print a chained list on the standard output
void affiche_li(chained_list * L)
{
	chained_list * P;
	P=L;
	printf("(");
	while(P!=NULL)
	{
		if((P->succ)!=NULL)
			printf("%d,",P->info);

		//if the loop is about to end, we don't display the ',' char after P->info
		else
			printf("%d",P->info);
		
		P=P->succ;
	}
	printf(")");
}


//insert a cell at the end of a chained_list
chained_list * ins_li(int e,chained_list * L)
{
	chained_list * P=L;
	if(P!=NULL)
	{
		while((P->succ)!=NULL)
		{
			P=P->succ;
		}
		P->succ=cons_li(e,NULL);
	}
	else
	{
		L=cons_li(e,NULL);
	}
	return L;
}

