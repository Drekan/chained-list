#ifndef CHAINED_LIST_H
#define CHAINED_LIST_H

//typedef allow us to set a pointer toward chained_list
//in the definition of chained_list itself
typedef struct chained_list chained_list;

//structure
struct chained_list{
	int info;
	chained_list * succ;
};

//build a single chained_list
chained_list * cons_li(int,chained_list *);

//display L
void affiche_li(chained_list * L);

//create a list of n cells
chained_list * create_li(int);

//insert a cell at the end of a chained_list
chained_list * ins_li(int,chained_list *);



#endif 