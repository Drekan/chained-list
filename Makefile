all:main.o chained_list.o
	gcc -Wall main.o chained_list.o -o x

main.o:main.c chained_list.h
	gcc -Wall -c main.c -o main.o

chained_list.o:chained_list.c
	gcc -Wall -c chained_list.c -o chained_list.o

c:
	rm -f *.o && rm -f x
