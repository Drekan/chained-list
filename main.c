#include <stdio.h>
#include <stdlib.h>

#include "chained_list.h"

int main(int nbParam, char ** paramLi)
{
	int taille;


	chained_list * L=cons_li(3,NULL);
	chained_list * M=cons_li(7,L);
	chained_list * N=cons_li(11,M);
	affiche_li(N);

	printf("\nQuelle taille de liste chaînée :");
	scanf("%d",&taille);

	chained_list * A;
	A=create_li(taille);
	affiche_li(A);
	A=ins_li(4,A);
	printf("\n");
	affiche_li(A);
	return 0;
}